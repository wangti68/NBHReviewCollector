# <center> Harvesting and Analyzing Neighborhood Data from Social Media </center>


# Program Description

Ultimately, I aim to build together a workflow to show how to scrape and analyze social media data and compare results from different social media sources with gold-standard traditional surveys. To be more specific, neighborhood information could be accessed by

-	Social environment and people’s perceptions of physical environment: tweets, neighborhood reviews on Niche.com, posts on Nextdoor.com, etc.;
-	Physical environment: Google Street View images, images on Instagram, etc. (this would be another project);
-	Traditional data source: American Community Survey, Census, and Behavioral Risk Factor Surveillance System.

For this course, I will start with one tiny piece of the above plan: downloading and identify physical activity related tweets by modifying the methods developed by [Nguyen et al. (2016)](https://publichealth.jmir.org/2016/2/e158/). In their project [Building a National Neighborhood Dataset From Geotagged Twitter Data for Indicators of Happiness, Diet, and Physical Activity](https://publichealth.jmir.org/2016/2/e158/), they created a list of 376 physical activities and used MALLET (a Java-based package for natural language processing) to classify tweets. I plan to follow their general logic but implement it in Python. In addition, I hope to be able to scrape reviews from Nextdoor and Niche, and apply the topic modelling method.
