"""A few functions for processing text data."""

import re
from datetime import datetime, timezone
import pandas as pd
import numpy as np

def ImportComment(file, text_column):
    """
    Load a csv file with comments, remove rows with empty text or illegible words or N/A answer.

    Input:
    file -- the name of a csv file;
    text_column -- the name of the target text column.
    Output:
    a Pandas dataframe.
    """
    raw = pd.read_csv(file, encoding="ISO-8859-1")
    df = raw[~raw[text_column].isnull()] # remove the rows where texts are missing
    df = df[df[text_column].str.contains("illegible|Illegible|N/A|NA|(no comment)") == False]
    return df

def TextCleaner(dirty_text):
    """
    Remove links, special characters, and numbers from text.

    Input:
    dirty_text -- a raw tweet or comment.
    Output:
    a clean tweet or comment.
    """
    dirty_text = dirty_text.replace("&amp", "")
    semi_clean_text = " ".join(re.sub(r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)",
                                      " ", dirty_text).split())
    clean_text = " ".join(re.sub(r"\d+", "", semi_clean_text).split()) # remove numbers
    return clean_text

def ImportTweet(file):
    """
    Load a csv file with tweets, drop all columns except time, location, and tweet content.

    Input:
    file -- the name of a csv file.
    Output:
    a Pandas dataframe with clean time (in UTC), location (latitude and longitude), and text.
    """
    raw = pd.read_csv(file, header=None)
    raw.columns = ["created_at", "id", "hashtags", "user_mentions", "in_reply_to_status_id",
                   "in_reply_to_user_id", "in_reply_to_screen_name", "username", "id",
                   "profile_location", "description", "user_url", "followers", "friends",
                   "user_created_at", "verified", "geo", "coordinates", "place",
                   "retweet", "favorite", "text"]
    raw = raw[["created_at", "coordinates", "text"]]
    # convert time format to UTC
    time = []
    for i in raw["created_at"]:
        time.append(datetime.strptime(i, '%a %b %d %H:%M:%S %z %Y').
                    replace(tzinfo=timezone.utc).astimezone(tz=None).strftime('%Y-%m-%d %H:%M:%S'))
    raw.insert(0, "time", time)
    raw["time"] = pd.to_datetime(raw["time"], utc=True)
    raw2 = raw.drop(columns="created_at")
    # clean up coordinates
    long = []
    lat = []
    for i in range(len(raw2)):
        num = re.findall(r"[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",
                         str(raw2["coordinates"].iloc[i]))
        if len(num) == 0:
            long.append(np.NaN)
            lat.append(np.NaN)
        else:
            long.append(float(num[0]))
            lat.append(float(num[1]))
    raw2.insert(2, "long", long)
    raw2.insert(2, "lat", lat)
    raw3 = raw2.drop(columns="coordinates")
    # clean text
    cleantweet = []
    for i in raw3["text"]:
        cleantweet.append(TextCleaner(i))
    raw3.insert(3, "tweet", cleantweet)
    df = raw3.drop(columns="text")
    return df
