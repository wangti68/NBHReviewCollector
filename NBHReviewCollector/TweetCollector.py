"""Collecting Twitter historical data using TwitterAPI."""

import csv
from TwitterAPI import TwitterAPI, TwitterPager

def HistoricalTweets(search_term, key, secret, access_token, access_token_secret,
                     from_date="201801010000", to_date="201912310000", filename="tweets.csv"):
    """
    Download tweets and write into a csv file.

    Input:
    search_term -- keywords for searching Twitter;
    key -- your Twitter API key;
    secret -- your API secret key;
    access_token -- your access token;
    access_token_secret -- your access token secret;
    from_date -- the start date (default "201801010000");
    to_date -- the end date (default "201912310000");
    filename -- name for the compiled csv file (default "tweets.csv").
    Output:
    A csv file of tweets downloaded in working directory.

    Reference:
    https://github.com/geduldig/TwitterAPI/tree/master/TwitterAPI
    https://developer.twitter.com/en/docs/tweets/search/overview
    """
    api = TwitterAPI(key,
                     secret,
                     access_token,
                     access_token_secret)

    request = api.request('tweets/search/%s/:%s' % ("fullarchive", "search"),
                          {'query': search_term,
                           'fromDate': from_date,
                           'toDate': to_date,
                           })

    csv_file = open(filename, 'a') # write tweets into a csv file
    csv_writer = csv.writer(csv_file)

    counter = 0
    for item in request:
        if 'extended_tweet' in item: # to avoid truncated tweets
            csv_writer.writerow([item['created_at'], item['user']['screen_name'],
                                 item['user']['location'], item['user']['followers_count'],
                                 item['user']['friends_count'], item['user']['verified'],
                                 item['coordinates'], item['retweet_count'],
                                 item['favorite_count'], item['extended_tweet']['full_text']])
        else:
            csv_writer.writerow([item['created_at'], item['user']['screen_name'],
                                 item['user']['location'], item['user']['followers_count'],
                                 item['user']['friends_count'], item['user']['verified'],
                                 item['coordinates'], item['retweet_count'],
                                 item['favorite_count'], item['text']])
        counter = counter + 1
        if counter == 100: # rate limit 100 responses per request
            break

    csv_file.close()
    print("{} is successfully created.".format(filename))
    return
